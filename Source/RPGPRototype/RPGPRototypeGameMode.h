// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RPGPRototypeGameMode.generated.h"

UCLASS(minimalapi)
class ARPGPRototypeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARPGPRototypeGameMode();
};



