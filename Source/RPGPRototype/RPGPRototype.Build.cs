// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class RPGPRototype : ModuleRules
{
	public RPGPRototype(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
