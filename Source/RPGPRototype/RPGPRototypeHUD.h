// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "RPGPRototypeHUD.generated.h"

UCLASS()
class ARPGPRototypeHUD : public AHUD
{
	GENERATED_BODY()

public:
	ARPGPRototypeHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

