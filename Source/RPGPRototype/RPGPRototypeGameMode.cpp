// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPGPRototypeGameMode.h"
#include "RPGPRototypeHUD.h"
#include "RPGPRototypeCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARPGPRototypeGameMode::ARPGPRototypeGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ARPGPRototypeHUD::StaticClass();
}
