// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueComponent.h"

// Sets default values for this component's properties

UDialogueComponent::UDialogueComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
}

void UDialogueComponent::InitializeComponent()
{
	Super::InitializeComponent();

	if(DialogDA && DialogDA->DialogObject)
	{
		DialogObject = DuplicateObject<UDialogue>(DialogDA->DialogObject, this);
		DialogObject->InitializeDialogue(this);
	}

}

void UDialogueComponent::OpenDialog()
{
	DialogObject->OpenDialog();
}

void UDialogueComponent::CloseDialog()
{
	DialogObject->CloseDialog();
}
void UDialogueComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}