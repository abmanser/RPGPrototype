// Fill out your copyright notice in the Description page of Project Settings.

#include "DialogueLines.h"

#include "Dialogue.h"
#include "DialogueComponent.h"
#include "Runtime/Core/Public/Async/ParallelFor.h"


void UBaseLine::InitializeParents(UBaseLine* TmpParentLine, UDialogue* AttachedDialogue)
{
	
}

void UNonPlayerLine::InitializeParents(UBaseLine* TmpParentLine, UDialogue* AttachedDialogue)
{
	SetParent(TmpParentLine);
	SetAttachDialogue(AttachedDialogue);

	if(IsValid(this->GetDialogLineData().NextLine))
		this->GetDialogLineData().NextLine->InitializeParents(this, AttachedDialogue);
}


void UPlayerMultiLine::InitializeParents(UBaseLine* TmpParentLine, UDialogue* AttachedDialogue)
{
	SetParent(TmpParentLine);
	SetAttachDialogue(AttachedDialogue);
	
	ParallelFor(GetDialogLineData(true).Num(), [&](int32 Idx)
		{
			FDialogLineData LineData = GetDialogLineData(true)[Idx];
			if (IsValid(LineData.NextLine))
				LineData.NextLine->InitializeParents(this, AttachedDialogue);
		}
	);
}

void UPlayerDecision::InitializeParents(UBaseLine* TmpParentLine, UDialogue* AttachedDialogue)
{
	SetParent(TmpParentLine);
	SetAttachDialogue(AttachedDialogue);

	ParallelFor(GetDialogLineData(true).Num(), [&](int32 Idx)
		{
			FDialogLineData LineData = GetDialogLineData(true)[Idx];
			if (IsValid(LineData.NextLine))
				LineData.NextLine->InitializeParents(this, AttachedDialogue);
		}
	);
}


UBaseLine* UBaseLine::GetNextDialogLine(FDialogLineData LineData)
{
	return nullptr;
}

UBaseLine* UPlayerMultiLine::GetNextDialogLine(FDialogLineData LineData)
{

	if (IsValid(LineData.Action))
	{
		if (GetAttachDialogue() && GetAttachDialogue()->GetAttachDialogueComp())
		{
			LineData.Action->DoActionOnLineUse(GetAttachDialogue()->GetAttachDialogueComp()->GetOwner());
			LineData.Action = nullptr;
		}
	}

	if (!IsValid(LineData.NextLine))
	{
		if (GetParent())
			GetParent()->TryGetParentLine(LineData);
	}


	return LineData.NextLine;
}

UBaseLine* UNonPlayerLine::GetNextDialogLine(FDialogLineData LineData)
{

	if (IsValid(LineData.Action))
	{
		if(GetAttachDialogue() && GetAttachDialogue()->GetAttachDialogueComp())
		{
			//Call Action that attached to this line.
			LineData.Action->DoActionOnLineUse(GetAttachDialogue()->GetAttachDialogueComp()->GetOwner());
			LineData.Action = nullptr;
		}
	}

	if(!IsValid(LineData.NextLine))
	{
		if(GetParent())
			return GetParent()->TryGetParentLine(LineData);
	}

	return LineData.NextLine;
}

UBaseLine* UPlayerDecision::GetNextDialogLine(FDialogLineData LineData)
{
	if (IsValid(LineData.Action))
	{
		if (GetAttachDialogue() && GetAttachDialogue()->GetAttachDialogueComp())
		{
			LineData.Action->DoActionOnLineUse(GetAttachDialogue()->GetAttachDialogueComp()->GetOwner());
			LineData.Action = nullptr;
		}
	}

	if (!IsValid(LineData.NextLine))
	{
		if (GetParent())
			GetParent()->TryGetParentLine(LineData);
	}
	return LineData.NextLine;
}


UBaseLine* UBaseLine::TryGetParentLine(FDialogLineData LineData)
{
	return nullptr;
}

UBaseLine* UNonPlayerLine::TryGetParentLine(FDialogLineData LineData)
{
	if (GetDialogLineData().bIsUsed)
	{
		if(IsValid(GetParent()))
			return GetParent()->TryGetParentLine(LineData);

		return nullptr;
	}
		

	return this;
}

UBaseLine* UPlayerMultiLine::TryGetParentLine(FDialogLineData LineData)
{
	int counter = 0;
	for (FDialogLineData Iter : GetDialogLineData(false))
	{
		if (Iter.bIsUsed)
			counter++;
	} 

	if(counter == GetDialogLineData(false).Num())
	{
		if (IsValid(GetParent()))
			return GetParent()->TryGetParentLine(LineData);

		return nullptr;
	}

	return this;
}


TArray<FDialogLineData> UPlayerMultiLine::GetDialogLineData(bool NeedBlocked) const
{
	TArray<FDialogLineData> NotBlockedDialogues;

	if (NeedBlocked)
		return NotBlockedDialogues = DialogLineData;

	for (int32 Idx = 0; Idx < DialogLineData.Num(); Idx++)
	{
		//Check if Line doesn't contain BlockCondition then add and continue
		if (!DialogLineData[Idx].BlockCondition) {
			NotBlockedDialogues.Add(DialogLineData[Idx]);
			continue;
		}

		//If BlockCondition exist, and not blocked then add
		if(DialogLineData[Idx].BlockCondition && !DialogLineData[Idx].IsBlocked(GetAttachDialogue()->GetAttachDialogueComp()->GetOwner()))
		{
			NotBlockedDialogues.Add(DialogLineData[Idx]);
		}
			
	}

	return NotBlockedDialogues;
}

TArray<FDialogLineData> UPlayerDecision::GetDialogLineData(bool NeedBlocked) const
{
	TArray<FDialogLineData> NotBlockedDialogues;

	if (NeedBlocked)
		return NotBlockedDialogues = DialogLineData;

	for (int32 Idx = 0; Idx < DialogLineData.Num(); Idx++)
	{
		if (!DialogLineData[Idx].BlockCondition) {
			NotBlockedDialogues.Add(DialogLineData[Idx]);
			continue;
		}
		if (DialogLineData[Idx].BlockCondition && !DialogLineData[Idx].IsBlocked(GetAttachDialogue()->GetAttachDialogueComp()->GetOwner()))
		{
			NotBlockedDialogues.Add(DialogLineData[Idx]);
		}
	}

	return NotBlockedDialogues;
}

UBaseLine* UPlayerDecision::TryGetParentLine(FDialogLineData LineData)
{
	for (FDialogLineData Iter : GetDialogLineData(false))
	{
		if (Iter.bIsUsed)
		{
			if (IsValid(GetParent()))
				return GetParent()->TryGetParentLine(LineData);

			return nullptr;
		}
	}
	return this;
}


