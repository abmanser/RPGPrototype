// Fill out your copyright notice in the Description page of Project Settings.


#include "Dialogue.h"

#include "DialogueComponent.h"
#include "Subsystems/SubsystemBlueprintLibrary.h"

void UDialogue::OpenDialog()
{
	//Call attached component open event
	AttachDialogueComp->OnOpen.Broadcast();

	const UDialogueSubSystem* Subsystem = Cast<UDialogueSubSystem>(USubsystemBlueprintLibrary::GetGameInstanceSubsystem(this, UDialogueSubSystem::StaticClass()));


	//If subsystem exist at this time, set that dialogue is opened and call Delegate about Opening
	if(IsValid(Subsystem))
	{
		bIsOpened = true;
		Subsystem->OnDialogOpenedDelegate.Broadcast(this);
	}

}

void UDialogue::CloseDialog()
{
	//Call attached component close event
	AttachDialogueComp->OnClose.Broadcast();

	const UDialogueSubSystem* Subsystem = Cast<UDialogueSubSystem>(USubsystemBlueprintLibrary::GetGameInstanceSubsystem(this, UDialogueSubSystem::StaticClass()));

	//If subsystem exist at this time, set that dialogue is opened and call Delegate about Opening
	if (IsValid(Subsystem))
	{
		bIsOpened = false;
		Subsystem->OnDialogClosedDelegate.Broadcast(this);
	}
}

UBaseLine* UDialogue::GetFirstDialogLine()
{
	if(bIsOpened)
		return InitialDialogLine;

	return nullptr;
}

UBaseLine* UDialogue::GetNextDialogLine(UBaseLine* CurrentLine, FDialogLineData LineData)
{
	if (IsValid(CurrentLine) && bIsOpened)
	{
		if (UBaseLine* BaseLine = CurrentLine->GetNextDialogLine(LineData))
		{
			InitialDialogLine = BaseLine;
			return BaseLine;
		}

			

		CloseDialog();
	}

	return nullptr;
}

void UDialogue::InitializeDialogue (UDialogueComponent* WhoCalled)
{
	AttachDialogueComp = WhoCalled;

	if(InitialDialogLine)
		InitialDialogLine->InitializeParents(nullptr, this);
}
