// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DialogueDA.h"
#include "Components/ActorComponent.h"
#include "DialogueComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_SPARSE_DELEGATE(FDialogueComponentOpenSignature, UDialogueComponent, OnOpen);
DECLARE_DYNAMIC_MULTICAST_SPARSE_DELEGATE(FDialogueComponentCloseSignature, UDialogueComponent, OnClose);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), NotBlueprintable)
class SSDIALOGUE_API UDialogueComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, Category = "Components|OnOpen")
		FDialogueComponentOpenSignature OnOpen;

	UPROPERTY(BlueprintAssignable, Category = "Components|OnClose")
		FDialogueComponentCloseSignature OnClose;

	// Sets default values for this component's properties
	UDialogueComponent();

	UFUNCTION(BlueprintCallable)
		void OpenDialog();

	UFUNCTION(BlueprintCallable)
		void CloseDialog();

	UFUNCTION(BlueprintCallable)
		void ChangeDialogue(UDialogueDA* _DialogDA) { DialogDA = _DialogDA; InitializeComponent(); }

protected:

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void InitializeComponent() override;


public:

	UPROPERTY()
		UDialogue* DialogObject;

	UPROPERTY(EditAnywhere)
		UDialogueDA* DialogDA;

};
