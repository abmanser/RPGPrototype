// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AdditionalClasses.h"
#include "UObject/NoExportTypes.h"
#include "DialogueLines.generated.h"

class UBaseLine;
class UDialogue;
/**
 * 
 */

UENUM(BlueprintType)
enum class EDialogLineType : uint8
{
	PlayerMulti,
	NonPlayer,
	PlayerDecision
};

USTRUCT(BlueprintType)
struct FDialogLineData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Instanced)
		UDialogueData* UIData;

	UPROPERTY(EditAnywhere, Instanced)
		UDialogueCondition* BlockCondition;

	UPROPERTY(EditAnywhere, Instanced)
		UDialogueAction* Action;

	UPROPERTY(EditAnywhere, Instanced)
		UBaseLine* NextLine;

	UPROPERTY(BlueprintReadOnly)
	bool bIsUsed = false;

	void SetIsUsed(bool _bIsUsed) { bIsUsed = _bIsUsed; }

	bool IsBlocked(AActor* WorldContext) const
	{
		return BlockCondition != nullptr ? BlockCondition->GetConditionResult(WorldContext) : false;
	}
};

UCLASS(BlueprintType)
class SSDIALOGUE_API UBaseLine : public UObject
{
	GENERATED_BODY()

public:

	UBaseLine() = default;

	UBaseLine(EDialogLineType _DialogLineType) : DialogLineType(_DialogLineType){}

	virtual void InitializeParents(UBaseLine* TmpParentLine, UDialogue* AttachedDialogue);
	virtual UBaseLine* GetNextDialogLine(FDialogLineData LineData);
	virtual UBaseLine* TryGetParentLine(FDialogLineData LineData);
	UBaseLine* GetParent() const { return ParentLine; }
	void SetParent(UBaseLine* _ParentLine) { ParentLine = _ParentLine; }
	UDialogue* GetAttachDialogue() const { return AttachDialogue; }
	void SetAttachDialogue(UDialogue* _AttachDialogue) { AttachDialogue = _AttachDialogue; }

	UPROPERTY(BlueprintReadOnly)
		EDialogLineType DialogLineType;
private:
	UPROPERTY()
		UBaseLine* ParentLine;

	UPROPERTY()
		UDialogue* AttachDialogue;
};

UCLASS(BlueprintType, EditInlineNew)
class SSDIALOGUE_API UNonPlayerLine : public UBaseLine
{
	GENERATED_BODY()

public:
	UNonPlayerLine()
		: UBaseLine(EDialogLineType::NonPlayer){}

	UFUNCTION(BlueprintCallable)
		void SetIsUsed(bool _bIsUsed) { DialogLineData.bIsUsed = _bIsUsed; }
	
	virtual UBaseLine* GetNextDialogLine(FDialogLineData LineData) override;
	virtual void InitializeParents(UBaseLine* TmpParentLine, UDialogue* AttachedDialogue) override;
	virtual UBaseLine* TryGetParentLine(FDialogLineData LineData) override;

	UFUNCTION(BlueprintCallable)
		FDialogLineData GetDialogLineData() const { return DialogLineData; }
private:

	UPROPERTY(EditAnywhere)
		FDialogLineData DialogLineData;
};

UCLASS(BlueprintType, EditInlineNew)
class SSDIALOGUE_API UPlayerMultiLine : public UBaseLine
{
	GENERATED_BODY()

public:
	UPlayerMultiLine()
		: UBaseLine(EDialogLineType::PlayerMulti){}

	 virtual void InitializeParents(UBaseLine* TmpParentLine, UDialogue* AttachedDialogue) override;
	 virtual UBaseLine* GetNextDialogLine(FDialogLineData LineData) override;
	 virtual UBaseLine* TryGetParentLine(FDialogLineData LineData) override;
	 
	 UFUNCTION(BlueprintCallable)
		 TArray<FDialogLineData> GetDialogLineData(bool NeedBlocked) const;

	UFUNCTION(BlueprintCallable)
		void SetIsUsed(int Variant, bool _bIsUsed) { DialogLineData[Variant].bIsUsed = _bIsUsed; }

private:

	UPROPERTY(EditAnywhere)
	TArray<FDialogLineData> DialogLineData;
};

UCLASS(BlueprintType, EditInlineNew)
class SSDIALOGUE_API UPlayerDecision : public UBaseLine
{
	GENERATED_BODY()

public:
	UPlayerDecision()
		: UBaseLine(EDialogLineType::PlayerDecision) {}

	virtual void InitializeParents(UBaseLine* TmpParentLine, UDialogue* AttachedDialogue) override;
	virtual UBaseLine* GetNextDialogLine(FDialogLineData LineData) override;
	virtual UBaseLine* TryGetParentLine(FDialogLineData LineData) override;

	UFUNCTION(BlueprintCallable)
		TArray<FDialogLineData> GetDialogLineData(bool NeedBlocked) const;

	UFUNCTION(BlueprintCallable)
		void SetIsUsed(bool _bIsUsed){
		ParallelFor(GetDialogLineData(true).Num(), [&](int32 Idx)
			{
			DialogLineData[Idx].bIsUsed = true;
			});
	}
private:

	UPROPERTY(EditAnywhere)
		TArray<FDialogLineData> DialogLineData;
};


