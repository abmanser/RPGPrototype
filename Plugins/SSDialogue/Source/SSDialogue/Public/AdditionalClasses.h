// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "AdditionalClasses.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable, EditInlineNew, Abstract, meta = (ShowWorldContextPin))
class SSDIALOGUE_API UDialogueData : public UObject
{
	GENERATED_BODY()

};

UCLASS(BlueprintType, Blueprintable, EditInlineNew, Abstract, meta = (ShowWorldContextPin))
class SSDIALOGUE_API UDialogueCondition : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
		bool GetConditionResult(AActor* WorldContext);
};

UCLASS(BlueprintType, Blueprintable, EditInlineNew, Abstract, meta = (ShowWorldContextPin))
class SSDIALOGUE_API UDialogueAction : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
		void DoActionOnLineUse(AActor* DialogueOwner);
};