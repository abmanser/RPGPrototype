// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Dialogue.h"
#include "Engine/DataAsset.h"
#include "DialogueDA.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, CollapseCategories)
class SSDIALOGUE_API UDialogueDA : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, Instanced, BlueprintReadOnly)
		UDialogue* DialogObject;

	UFUNCTION(BlueprintCallable)
		UDialogue* GetDialogueObject() { return DialogObject; }
};
