// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DialogueSubSystem.h"
#include "CoreMinimal.h"
#include "DialogueLines.h"
#include "UObject/NoExportTypes.h"
#include "Dialogue.generated.h"

/**
 * 
 */

class UDialogueComponent;

UCLASS(BlueprintType, EditInlineNew)
class SSDIALOGUE_API UDialogue : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, Instanced)
		UBaseLine* InitialDialogLine;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Instanced)
		TArray<UDialogueData*> VariosOfDialogueEndingsData;

	void InitializeDialogue(UDialogueComponent* WhoCalled);

	UDialogueComponent* GetAttachDialogueComp() { return AttachDialogueComp;  }
private:

	bool bIsOpened = false;

	UPROPERTY()
		UDialogueComponent* AttachDialogueComp;
public:


	UFUNCTION(BlueprintCallable)
		void OpenDialog();

	UFUNCTION(BlueprintCallable)
		void CloseDialog();

	UFUNCTION(BlueprintCallable)
		UBaseLine* GetFirstDialogLine();
	
	UFUNCTION(BlueprintCallable)
		UBaseLine* GetNextDialogLine(UBaseLine* CurrentLine, FDialogLineData LineData);

	UFUNCTION(BlueprintGetter)
		bool IsOpened() const { return bIsOpened; }

};
