// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "DialogueSubSystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDialogActionDelegate, UDialogue*, Dialog);
/**
 * 
 */
UCLASS()
class SSDIALOGUE_API UDialogueSubSystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	// virtual void Deinitialize() override;

	UPROPERTY(BlueprintAssignable)
		FOnDialogActionDelegate OnDialogOpenedDelegate;

	UPROPERTY(BlueprintAssignable)
		FOnDialogActionDelegate OnDialogClosedDelegate;
};
