// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestComponent.h"

#include "QuestSubSystem.h"
#include "Subsystems/SubsystemBlueprintLibrary.h"

class UQuestSubSystem;


UQuestComponent::UQuestComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
}

void UQuestComponent::InitializeComponent()
{
	UQuestSubSystem* Subsystem = Cast<UQuestSubSystem>(USubsystemBlueprintLibrary::GetGameInstanceSubsystem(this, UQuestSubSystem::StaticClass()));
	if (IsValid(Subsystem))
		Subsystem->RegisterQuest(this);
}

void UQuestComponent::SetQuestActive()
{
	bQuestIsActive = true;
}

void UQuestComponent::SetQuestIsSolved()
{

	bQuestIsSolved = true;

	const UQuestSubSystem* Subsystem = Cast<UQuestSubSystem>(USubsystemBlueprintLibrary::GetGameInstanceSubsystem(this, UQuestSubSystem::StaticClass()));
	if (IsValid(Subsystem))
		Subsystem->OnQuestSolvedDelegate.Broadcast(this);

	OnSolved.Broadcast();
}





