// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestSubSystem.h"

void UQuestSubSystem::RegisterQuest(UQuestComponent* Quest)
{
	if(IsValid(Quest))
		QuestStorage.Add(Quest);
}

TArray<UQuestComponent*> UQuestSubSystem::GetAllActiveQuests()
{
	TArray<UQuestComponent*> ActiveQuests;

	for (UQuestComponent* Iter : QuestStorage)
	{
		if (Iter->GetIsQuestActive())
			ActiveQuests.Add(Iter);

	}
	return ActiveQuests;
}

UQuestComponent* UQuestSubSystem::FindQuestByKeyWord(FString KeyWord) {
	return (UQuestComponent*) QuestStorage.FindByPredicate([&](UQuestComponent* x){ return x->GetKeyWord().Equals(KeyWord); });
}

