// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QuestComponent.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "QuestSubSystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnQuestSolvedDelegate, UQuestComponent*, Quest);
/**
 * 
 */
UCLASS()
class SSQUEST_API UQuestSubSystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
		UPROPERTY(BlueprintAssignable)
			FOnQuestSolvedDelegate OnQuestSolvedDelegate;

		void RegisterQuest(UQuestComponent* Quest);

		UFUNCTION(BlueprintCallable)
		TArray<UQuestComponent*> GetAllActiveQuests();
		
		UFUNCTION(BlueprintCallable)
			UQuestComponent* FindQuestByKeyWord(FString KeyWord);
private:
	UPROPERTY()
		TArray<UQuestComponent*> QuestStorage;
};
