// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "QuestComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_SPARSE_DELEGATE(FQuestComponentSolvedSignature, UQuestComponent, OnSolved);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SSQUEST_API UQuestComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UPROPERTY(BlueprintAssignable, Category = "Components|OnSolved")
	FQuestComponentSolvedSignature OnSolved;

	UQuestComponent();
	
	virtual void InitializeComponent() override;

	UFUNCTION(BlueprintCallable)
		void SetQuestActive();

	UFUNCTION(BlueprintCallable)
		void SetQuestIsSolved();

	bool GetIsQuestActive() const { return bQuestIsActive; }

	UFUNCTION(BlueprintCallable)
	bool GetIsQuestSolved() const { return bQuestIsSolved; }
// private:

	UFUNCTION(BlueprintCallable)
		FString GetDescription() const { return Description; }
	
	UFUNCTION(BlueprintCallable)
		FString GetKeyWord() const { return KeyWord; }

	UFUNCTION(BlueprintCallable)
		FString GetQuestName() const { return QuestName; }
private:
	UPROPERTY(EditAnywhere)
		FString QuestName;

	UPROPERTY(EditAnywhere)
		FString Description;

	UPROPERTY(EditAnywhere)
		bool bQuestIsActive;

	UPROPERTY(EditAnywhere)
		bool bQuestIsSolved;

	UPROPERTY(EditAnywhere);
		FString KeyWord;
};
